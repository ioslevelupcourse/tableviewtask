//
//  AddPersonViewController.h
//  TableViewTask
//
//  Created by Admin on 16.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddPersonViewController : UIViewController <UITextFieldDelegate>

@end
