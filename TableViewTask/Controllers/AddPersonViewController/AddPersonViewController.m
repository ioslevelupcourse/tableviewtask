//
//  AddPersonViewController.m
//  TableViewTask
//
//  Created by Admin on 16.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "AddPersonViewController.h"
#import "ActionSheetPickerView.h"
#import "ActionSheetPickerData.h"

#import "LUPerson.h"
#import "AppHelper.h"

#import "TableViewController.h"

#pragma mark - Constants Privat
NSString *const msgEnterAge = @"Enter age";
NSString *const msgCheckEnteredAgeAgain = @"Check the entered age again";
NSString *const msgCheckEnteredDataAgain = @"Check the entered data again";
NSString *const msgPostAnIncorrectEntry = @"Post an incorrect entry";
NSString *const msgEmptyInput = @"Empty input";
NSString *const msgMoreThanTwoCharacters = @"More than two characters";
NSString *const msgNoMoreThanCharacters = @"No more than 40 characters";

@interface AddPersonViewController ()

@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *countryTextField;
@property (weak, nonatomic) IBOutlet UITextField *ageTextField;
@property (weak, nonatomic) IBOutlet UISwitch *sexSwitch;
@property (weak, nonatomic) IBOutlet UIButton *showButton;

@property (nonatomic, strong) ActionSheetPickerView *pickerView;
@property (nonatomic, strong) ActionSheetPickerData *pickerData;

@property (nonatomic, strong) LUPerson *person;
@property (nonatomic, assign) BOOL isKeyboardOpen;

@end

@implementation AddPersonViewController

#pragma mark - View life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.person = [LUPerson new];
    self.isKeyboardOpen = NO;
    
    [self.sexSwitch setOn:NO animated:NO];
    [self.firstNameTextField becomeFirstResponder];
}

#pragma mark - Actions
- (IBAction)showButtonDidClick:(UIButton*)sender {
    NSString* message = nil;
    if ([AppHelper isValidSymbolsForName: self.firstNameTextField.text]  &&
        [AppHelper isValidSymbolsForName: self.lastNameTextField.text]) {
        self.person.firstName   = self.firstNameTextField.text;
        self.person.lastName    = self.lastNameTextField.text;
        self.person.sex         = self.sexSwitch.on;
        self.person.country     = self.countryTextField.text;
        self.person.age         = [AppHelper dateFromString:self.ageTextField.text];

        TableViewController * tableViewController = [self.navigationController.viewControllers firstObject];
        [tableViewController.persons addObject:self.person];
        
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        if (![AppHelper isValidSymbolsForAge: self.ageTextField.text]) {
            message = msgCheckEnteredAgeAgain;
        } else {
            message = msgCheckEnteredDataAgain;
        }
    }
    [self presentAlertDataImputWithMessage:message
                                controller:self];

}

#pragma mark - Private
- (void)becomeFirstResponderCountryTextField {
    [self.firstNameTextField resignFirstResponder];
    [self.lastNameTextField resignFirstResponder];
    [self.ageTextField resignFirstResponder];
}

- (void)becomeFirstResponderAgeTextField {
    [self.firstNameTextField resignFirstResponder];
    [self.lastNameTextField resignFirstResponder];
    [self.countryTextField resignFirstResponder];
}

- (void)pickerViewShow {
    self.pickerView = [ActionSheetPickerView stringPickerWithItems:[AppHelper countryNames] startSelectItem:self.countryTextField.text selectCallBack:^(id selected) {
        NSLog(@"selected: %@", selected);
        if (selected) {
            self.countryTextField.text = [NSString stringWithFormat:@"%@", selected];
            [self.ageTextField becomeFirstResponder];
        } else {
            NSLog(@"Canceled");
        }
        self.pickerView = nil;
    }];
    
    [self.pickerView presentPicker];
    
}

- (void)pickerDataShow {
    NSDate *data = [AppHelper dateFromString:self.ageTextField.text];
    self.pickerData =
    [ActionSheetPickerData pickerDataWithStartSelectItem:data selectCallBack:^(id selected) {
                                              NSLog(@"selected: %@", selected);
                                              [self resultOnPickerDataSelected:selected];
                                              self.pickerData = nil;
     }];
    [self.pickerData presentPicker];
}

- (void)resultOnPickerDataSelected:(id)selected {
    if (selected) {
        if ([selected isKindOfClass:[NSDate class]]) {
            self.ageTextField.text = [AppHelper stringFromDate:selected];
        } else {
            self.ageTextField.text = [NSString stringWithFormat:@"%@", selected];
        }
        [self.showButton becomeFirstResponder];
    } else {
        NSLog(@"Canceled");
    }
}

- (void)presentAlertDataImputWithMessage:(NSString *)message
                              controller:(UIViewController *)controller {
    [self presentAlert:@"Data Input"
               message:message
            controller:controller];
}

- (void)presentAlert:(NSString *)title
             message:(NSString *)message
          controller:(UIViewController *)controller {
    UIAlertController* alertView = [UIAlertController alertControllerWithTitle: title
                                                                       message: message
                                                                preferredStyle: UIAlertControllerStyleAlert];
    [alertView addAction:[UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                handler:nil]];
    [controller presentViewController:alertView
                             animated:true
                           completion:nil];
}

#pragma mark - <UITextFieldDelegate>
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    //pickerView
    if ([textField isEqual:self.countryTextField]) {
        [self becomeFirstResponderCountryTextField];
        if (self.pickerData) {
            [self.pickerData dismissPicker];
            self.pickerData = nil;
        }
        if (!self.pickerView) {
            [self pickerViewShow];
        }
        return NO;
    } else if (self.pickerView) {
        [self.pickerView dismissPicker];
        self.pickerView = nil;
    }
    
    //pickerData
    if ([textField isEqual:self.ageTextField]) {
        [self becomeFirstResponderAgeTextField];
        if (self.pickerView) {
            [self.pickerView dismissPicker];
            self.pickerView = nil;
        }
        if (!self.pickerData) {
            [self pickerDataShow];
        }
        return NO;
    } else if (self.pickerData) {
        [self.pickerData dismissPicker];
        self.pickerData = nil;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    BOOL result = YES;
    NSString* message = nil;
    if ([textField isEqual:self.ageTextField]) {
        if (self.ageTextField.text == NULL || self.ageTextField.text.length == 0) {
            message = msgEnterAge;
            result = NO;
        } else if (![AppHelper isValidSymbolsForAge:self.ageTextField.text]) {
            message = msgPostAnIncorrectEntry;
            result = NO;
        }
    } else {
        if (textField.text == NULL || textField.text.length == 0) {
            message = msgEmptyInput;
            result = NO;
        } else if (textField.text.length <= 2) {
            message = msgMoreThanTwoCharacters;
            result = NO;
        } else if (textField.text.length > 40) {
            message = msgNoMoreThanCharacters;
            result = NO;
        } else if (![AppHelper isValidSymbolsForName: textField.text]) {
            message = msgPostAnIncorrectEntry;
            result = NO;
        }
    }
    if (result) {
        if ([textField isEqual:self.firstNameTextField]) {
            [self.lastNameTextField becomeFirstResponder];
        } else if ([textField isEqual:self.lastNameTextField]) {
            [self.countryTextField becomeFirstResponder];
        } else if ([textField isEqual:self.countryTextField]) {
            [self.ageTextField becomeFirstResponder];
        }
    } else {
        [self presentAlertDataImputWithMessage:message
                                    controller:self];
    }
    return result;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range
                                                                  withString:string];
    if (newString.length == 0) return YES;
    if (textField == self.ageTextField) {
        return [AppHelper isValidSymbolsForAge:newString];
    } else {
        return [AppHelper isValidSymbolsForName:newString];
    }
    return YES;
}

- (CGFloat)takeHeightKeyboardOfUserInfo:(NSDictionary*)dictionary {
    CGRect keyboardFrame = [dictionary [UIKeyboardFrameEndUserInfoKey] CGRectValue];
    double height = keyboardFrame.size.height;
    return height;
}

@end
