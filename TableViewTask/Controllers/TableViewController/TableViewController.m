//
//  TableViewController.m
//  TableViewTask
//
//  Created by Admin on 17.04.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "TableViewController.h"
#import "LUPerson.h"
#import "UITableView+LUExtentions.h"

@interface TableViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation TableViewController

#pragma mark - View life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.persons = [NSMutableArray new];
    [self.persons addObject:[LUPerson personWithFirstName:@"An0" lastName:@"Pet0"]];
    [self.persons addObject:[LUPerson personWithFirstName:@"An2" lastName:@"Pet2"]];
    [self.persons addObject:[LUPerson personWithFirstName:@"An3" lastName:@"Pet3"]];
    
    UIView *rectZero = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableHeaderView = rectZero;
    self.tableView.tableFooterView = rectZero;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self presentAlert:@"PERSONS.COUNT"
               message:@(self.persons.count).stringValue
            controller:self];
    [self.tableView reloadData];
}

#pragma mark - Privat
- (void)presentAlert:(NSString *)title
             message:(NSString *)message
          controller:(UIViewController *)controller {
    UIAlertController* alertView = [UIAlertController alertControllerWithTitle: title
                                                                       message: message
                                                                preferredStyle: UIAlertControllerStyleAlert];
    [alertView addAction:[UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleDefault
                                                handler:nil]];
    [controller presentViewController:alertView
                             animated:true
                           completion:nil];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (self.persons.count + 1);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (indexPath.row < self.persons.count) {
        cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([LUPerson class])];
        LUPerson *person = self.persons[indexPath.row];
        cell.textLabel.text = [person allPropertyToString];
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
    }
    return cell;
}

#pragma mark - UITableViewDelegate methods
- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *action = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Delete" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        [self removePersonAtIndexPath:indexPath];
    }];
    
    return @[action];
}

- (void)removePersonAtIndexPath:(NSIndexPath *)indexPath {
    [self.persons removeObjectAtIndex:indexPath.row];
    __weak typeof(self) weakSelf = self;    
    [self.tableView perfomUpdates:^{
        [weakSelf.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.row < self.persons.count;
}

#pragma mark - Public
@end
