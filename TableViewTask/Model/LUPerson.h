//
//  LUPerson.h
//  TableViewTask
//
//  Created by Admin on 06.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LUPerson : NSObject

@property(nonatomic, copy) NSString *firstName;
@property(nonatomic, copy) NSString *lastName;
@property(nonatomic, copy) NSString *country;
@property(nonatomic, assign) BOOL sex;
@property(nonatomic, strong) NSDate *age;

+ (LUPerson *)personWithFirstName:(NSString *)firstName lastName:(NSString *)lastName;
- (instancetype)initWithFirstName:(NSString *)firstName lastName:(NSString *)lastName;

- (NSString *)allPropertyToString;

@end
