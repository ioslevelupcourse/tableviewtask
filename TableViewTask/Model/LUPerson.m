//
//  LUPerson.m
//  TableViewTask
//
//  Created by Admin on 06.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import "LUPerson.h"
#import "AppHelper.h"

@implementation LUPerson
#pragma mark - Initialize
- (instancetype)initWithFirstName:(NSString *)firstName lastName:(NSString *)lastName {
    self = [super init];
    if (self) {
        _firstName = firstName;
        _lastName = lastName;
        _sex = YES;
        _country = @"Ukr";
        _age = [NSDate date];
    }
    
    return self;
}

+ (LUPerson *)personWithFirstName:(NSString *)firstName lastName:(NSString *)lastName {
    return [[LUPerson alloc] initWithFirstName:firstName lastName:lastName];
}

#pragma mark - Public
- (NSString *)allPropertyToString {
    NSString* string = [NSString stringWithFormat:
                        @"%@ %@ %@ %@ %@",
                        self.firstName,
                        self.lastName,
                        self.sex ? @"Famale" : @"Male",
                        self.country,
                        [AppHelper stringFromDate:self.age]];
    
    return string;
}

@end
