//
//  AppHelper.h
//  TextFieldTask
//
//  Created by Admin on 16.03.17.
//  Copyright © 2017 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AppKeyboardNotifications <NSObject>
@required
- (void)keyboardWillShow:(NSNotification *)notificion;
- (void)keyboarWillHide:(NSNotification *)notificion;
@end

@interface AppHelper : NSObject

//add @protocol <AppKeyboardNotifications>
+ (void)subscribeToKeyboardNotificationsObject:(id)object;
+ (void)unsubscribeFromKeyboardNotificationsObject:(id)object;

//Other method
+ (BOOL)isValidSymbolsForName:(NSString *)name;
+ (BOOL)isValidSymbolsForAge:(NSString *)string;
+ (BOOL)stringIsEmpty:(NSString *)string;

+ (NSArray *)countryNames;
+ (NSString *)stringFromDate:(NSDate *)dataBirthday;
+ (NSDate *)dateFromString:(NSString *)string;

@end
